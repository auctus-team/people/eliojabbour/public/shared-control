# A activity-based linear blending- Shared Control-

## Architecture
In order to understand the code and change the variables :
Lets introduce the block diagram on what this code is composed of :
![Alt Text](Pictures/SC.drawio.png)
## Haptic Device $`x_{h}`$
The haptic device is used by the human to send a position to the robot.
We can either use the falcon or the omega 7 haptic device.
A ROS node is created for falcon where we subscribe to "/falcon/joystick" and get "falcon_joy_msg" as a message for the human position. To send force to the haptic device we can use "ros_falcon::falconForces cartesian_force_ms;"

The haptic position $\mathbf{x_{h}}$ sent by the human in the robot workspace is computed as :

$\mathbf{x_{h}} =   s_{t}(\mathbf{x_{p}}-\mathbf{x_{c}})+\mathbf{x_{i}}$

where $\mathbf{x_{i}}$ is the initial pose of the robot in the robot workspace, $\mathbf{x_{c}}$ is the center pose of the haptic device and $\mathbf{x_{p}}$ is the position of the haptic interface  in the haptic workspace at the current time. $s_{t}$ the transitional scaling factor.
In the code Version :

```cpp
HapticPosition[]= s_t *(falcon_joy_msg.axes[]) - xh_center[]) + xr_initial[];
```

## Intent Recognition $`x_{r}`$
For the robot to estimate where the human is going, we predict it by the distance between the robot-end effector and the object  "+" velocity direction of the haptic device in the robot workspace. Thus, we formulate it as :

$` \mathbf{{p}_{intent}}=0.5\mathbf{{p}_{dot}}+0.5\mathbf{{p}_{dist}}`$

where $`\mathbf{{p}_{dot}}`$ is a function that give a value between [0,1] when the human is going toward the target it is "0" and "1" when opposite.

and   $`\mathbf{{p}_{dist}}`$ is a function that give a value "0" when near a target and "~1" when upmost away from the workspace.

The robot target $`\mathbf{x_{r}}`$ where the human is trying to go will become the object index that has the minium $`\mathbf{{p}_{intent}}`$ :


```cpp
void TorqueController::IntentRecognition(Eigen::Matrix<double, 3, 3> DesiredOpPos, Eigen::Vector3d Velocity_H, Eigen::Vector3d xPos,
  Eigen::Vector3d &Intentestimate_odePrev, Eigen::Vector3d &D_Intentestimate_ode, double Kt_intent, string &BoxObj, int &index1)
//.................... INPUT: ...............................................//
// Object position from OptiTrack:DesiredOpPos...............................//
// Velocity Direction of Haptic : Velocity_H.................................//
// Robot current task position : cartesian_pose.translation()................//
// Intent value of previous time : Intentestimate_odePrev....................//
// D_Intentestimate_ode : Intent derivation..................................//
// Kt_intent (time coefficient for the intent to converge)...................//
// Previous Object that was detected.........................................//
// Object name : BoxObj (Not necessary)......................................//
// Index of the object to be sent to DesiredOpPos : index1 (Not necessary)...//

```
\ TO DO show the display of objects vs intent  as GIF

## Authority Level: $`\beta`$
Our method is a storing-tank technique that links the authority/blending factor to the human's activity. The activity level of the human is measured through its power generated by the haptic device and stores it in a the tank over time. The power of the human $P_{h}$ is represented can be represented by:
$` P_{h}= {F_{h}}^{T}\dot{x}_{h}`$
We can approximate $`F_{h}`$  by measuring the acceleration of the handler $`\ddot{x}_h`$  assuming that the gravity compensation is active, and the system being considered a unit mass $`m`$.
Thus, power produced by the human is equivalent to:
  $`P_{h}= m \ddot{x}_{h}\dot{x}_{h}`$.

We want to calculate an input to this tank where it is related to activity and the tank height(authority level) have different variation depending on the input. The authority level $`\beta`$ is modeled as :

$`K_{t}\frac{d\beta}{dt}+\beta= \beta_{in}`$

where the factor $K_{t}$ can varies the speed of variation in the tank increase or decrease.

Now to calculate $`\beta_{in}`$, There several method that can be done to go from the human power to an activity based authority level :

### Step Minimum power :
In this method, we assume that any minimum activity exerted by the human is perceived as an activity, and thus, the human retains instantaneous authority. We measure the minimum power that can be observed when the human intends to initiate motion. The authority input is computed as follows:
$`
\beta_{in} = \begin{cases}
    1  & \text{if } P_{h} \geq P_{min} \\
    0 & \text{if } P_{h} < P_{min}
\end{cases}
`$




```cpp

        StepBasedAuthority(CurrentPower, Minimum Power, Kt_step, delta_t);
        //..................................................//

        //.................... INPUT: ........................//
        // CurrentPower:Haptic device Power at the instant time
        // MinimumPower: Minimum Power to hint for activity...//
        // Kt_step : Time Coefficient.........................//
        // delta_t: step time.................................//


```
Here we show different $`Kt`$ variables and input for a specific movement :

![Alt Text](Pictures/StepPower_different KT.png)

From the results we can choose Kt depending on the user choice, however based on the results of preleminaries test, $`K_{t}=0.5`$ is recommended. If you want the human/robot to retake control faster and choose lower k_{t}. If you want a smnooth yet slower response time for  authority variation, choose higher $`K_{t}`$

### Linear Power [Pre-defined Min/Max] :
The idea here is to calculate $P_{max}$ and P_{min} and then calculate a level of input activity $`\beta_{in}`$ that can be interpolated between these 2 values.
$`B_{in}= \frac{P-P_{min}}{P_{max}-P_{min}}`$

One approach is to calculate the maximum power that can be done on the haptic device from one end to another  and the minimum power to make motion without being stall.
Average Values for the  $`P_{max}=1.2`$ and $`P_{min}=0.03`$

```cpp
LinearMinMaxAuthority(CurrentPower, MinimumPower, MaximumPower, Kt_linear, delta_t)

        //.................... INPUT: .................................//
        // CurrentPower:Haptic device Power at the instant time........//
        // MinimumPower: Minimum Power to hint for activity............//
        // MaximumPower : Average Power throughout all the experiment..//
        // Kt_linear:Time Coefficient..................................//
        // delta_t: step time..........................................//
```

![Alt Text](Pictures/MinMax.png)

From the graph we can see that this method is more smooth in terms of authority control shift yet it is difficult to reach max authority level.


### Linear Global Power:
Similiar to the last method but the  idea  here is to calculate  the maximum that corresponds to each different user, thus calculating the average usage as the maximum authority.
In order to estimate the average activity made by the human to do a specific motion and then we assume that that an average motion made by the human should be equivalent to $`P_{max}`$ :

$`P_{mean}=P_{max}=\frac{1}{t}\sum_{t=0}^{t=current}P`$

```cpp
void TorqueController::GlobalLinearAuthority(double CurrentPower, double MinimumPower,
   double GlobalPowerMean, double Kt_global, double delta_t)s
   //.................... INPUT: .......................//
   // CurrentPower:Haptic device Power at the instant time
   // MinimumPower: Minimum Power to hint for activity...//
   // GlobalPowerMean : Average Power throughout all the
   // experiment
   // Kt_global:Time Coefficient.........................//
   // delta_t: step time.................................//
   //...................................................//

```
![Alt Text](Pictures/GlobalMean.png)



// ### EnergyBasedAuthority:
// The idea here is to convert the power generated to energy with time:
// $`E_{i}=\int^{t_{i}}_{t_{i-1}} P dt`$
// Then we compare the current energy generated with the energy of the previous time-frame and we increase/decrease the authority level based on the energy difference :
//
// $` /B_{in} = \begin{cases} /B+k_{t} & \text{if } E_{2} \geq E_{1} \\ /B+k_{t} & \text{if } E_{2} < E_{1} \end{cases} `$
//
// \TO DO
### Mean excluding zeros
### StandardDeviation
### EnergyBasedAuthority

 ### Results and comparison of each activity method
//
// \TO DO


## Linear Blending: $`x_{d}`$:

In linear blending, the inputs from the human and robot are assigned weights which is calculated in the authority level part that determine their influence on the final control output. These weights are typically scalar values between 0 and 1, representing the relative importance  of each input.
The blending process works by multiplying each input by its corresponding weight and then summing them together. The resulting sum represents the combined control signal that drives the robot's actions:

$`x_{d}=\beta x_{h}+(\beta-1)x_{r}`$

```cpp
Eigen::Vector3d TorqueController::Blending(double AuthorityLevel,
  Eigen::Vector3d Position_H, Eigen::Vector3d Position_R, Eigen::Matrix<double, 6, 1> WallDimension)

  //.................... INPUT: .......................//
  // AuthorityLevel: it is the level of authority of the human calculated
  by one of the methods above
  // Position_H:Haptic position of the human.............//
  // Position_R : Objected detected position ............//
  // WallDimension: Dimension of the workspace to restrict
   robot going outside..................................//
   //.................... OUTPUT: .......................//
   // PositionCommand: The linear blended position........//
  //...................................................//
```
## PD : $`\dot{v}_{des}`$
The desired acceleration $`\ddot{x}_{d}`$ is computed through a PD controller that takes as input, a desired blended position $`x_{d}`$  and the robot current pose $`x`$ and the current velocity as $`\dot{x}`$.
Therefore the desired acceleration to minimize in the QP is :
    $` \ddot{x}_{d}= \ddot{x}-\frac{\mathbf{K_{p}}}{\mathbf{K_{v}}}(\mathbf{x}-\mathbf{x_{d}}) `$

To saturate and limit the velocity term in respect to the task constraints, we use $\mathbf{V}$ as a scaling factor and $\dot{x}_{max}$ as the maximum velocity limits.

```cpp
Eigen::Matrix<double, 6, 1> TorqueController::SaturateVelocity(Eigen::Vector3d saturated_linear_velocity,
  Eigen::Vector3d saturated_angular_velocity, Eigen::Matrix<double, 6, 1> err, Eigen::VectorXd d_gains,
  Eigen::VectorXd p_gains,pinocchio::Motion Xd,Eigen::Matrix<double, 6, 1> xdd)

  //.................... INPUT: .......................//
  // saturated_linear_velocity: Max task velocity
  // saturated_angular_velocity: Max task angular velocity
  // err: Position error between desired and current.
  // d_gains : Damping coefficient of the PD controller ............//
  // p_gains : Proportional coefficient of the PD controller ............//
  // Xd: Current velocity of the robot.............//
  // xdd: Current acceleration of the robot.............//
  //...................................................//
  //.................... OUTPUT: .......................//
  // xdd_star: task space desired acceleration.........//
  //...................................................//


```
