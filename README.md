# A activity-based linear blending- Shared Control-
## Context

Welcome Stalker Pilots of Gitlab to my basic shared control demo.

To implement this method manually, several key variables need to be understood. The haptic device serves as the interface between the human and the robot, facilitating the exchange of position control. The intent recognition  calculates the human's intended target by analyzing the direction and distance factors. Lastly, the authority level, determined by the human's activity, governs the balance between human and robot control.


## Installation
You can clone this workspace or install it manually as follows :


The code use the QP controller of Auctus  where you need to use the panda_torque_qp, the falcon haptic device and the Optitrack;
The installation procedure is explained here :
- [ ] [ElioBranch in Panda QP workspace](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_ws)
- [ ] [OptiTrack](https://gitlab.inria.fr/auctus-team/components/sensors/cameras/optitrack/optitrack_to_robot)

### FALCON Haptic drivers

```

sudo apt-get install libusb-1.0.0-dev
git clone https://github.com/libnifalcon/libnifalcon.git
mkdir build
cd build
cmake -G "Unix Makefiles" ..
make
make install
roscd falcon
sudo cp udev_rules/99-udev-novint.rules /etc/udev/rules.d

<!-- You need to unplug and replug the device. You may also need to restart the OS -->
```
Also install the ros_falcon library from :
- [ ] [ros_falcon](https://github.com/WPI-AIM/ros_falcon.git)



## Content

[Content](./CONTENT.md)


## RUN ME

[Modifiable parameters & RUN ME](./RUN_ME.md)
