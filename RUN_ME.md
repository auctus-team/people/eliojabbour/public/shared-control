# A activity-based linear blending- Shared Control-
## Context
The purpose of this file is to explain what kind of parameters can be modified that are related to the experiment and the behavior expected in different parameters.
Most of the code is in `panda_qp_ws/src/panda_qp_control/src/panda_torque_control.cpp`

NOTE: here we only explain modiable parameters and that the methods and variables are more detailed  in the content

## The Haptic controller

```cpp
FilteredJoy=FilteringPosition(FILTER_SIZE,position_queue,position_sum,JoyPos);
```
| Variables | Description |
| --- | --- |
| FILTER_SIZE | defaults size= 100.=~ 0.1s. This is the size of the moving filter to calculate the filtered position. Increase it if you want more smooth yet slower response time|

To change the acceleration 2nd order butterworth filter behavior :
```cpp
pii = tan((Freq_cuttoff / 730.0) * (M_PI / 2.0));
```
| Variables | Description |
| --- | --- |
| pii | this is the cutt-off ratio. default value 20.0/730.0   |

```cpp
void TorqueController::PositionCallback(const sensor_msgs::Joy &falcon_joy_msg)
{
  ...
HapticPosition[0] = s_t * ((FilteredJoy[0]) - haptic_center[0]) + initial_pose[0];
HapticPosition[1] = s_t * (FilteredJoy[1]-haptic_center[1]) + initial_pose[1];
HapticPosition[2] = s_t * (FilteredJoy[2]-haptic_center[2]) + initial_pose[2];
 ...
}
```

| Variables | Description |
| --- | --- |
| s_t | Translation factor that can increase or decrease the workspace of the robot. Default value is 4.0 |
| initial_pose | This is the initial position of the robot when the haptic device is in the center, change it when you need to change the area of manipulation. Default value=[0.3,0.6,0.85] |
| haptic_center | This is the center of the haptic device workspace.Default value=[0.125,0.0,0.0]|
| FilteredJoy | This is the current position of the haptic that is filtered by a moving mean filter|


## Intent Recognition
```cpp
IntentRecognition(DesiredOpPos, Velocity_H, cartesian_pose.translation(), Intentestimate_odePrev, D_Intentestimate_ode, k_intent, BoxObj, index1);
```
| Variables | Description |
| --- | --- |
| DesiredOpPos | This is the matrix of the objects positions. Change it when it is pre-defined. Each row has different object xyz position. Dont change it when it is given by optitrack |
| k_intent | this is the time coefficient same function as the tank-like system to make the prediction evolves in a smoother behavior. Decrease it when you want the prediction to be faster but you might lose accuracy. Increase when you want it to be more smooth and accurate but it will be slower to detect an object. Optimal values  are between 0.5 and 1.0|


## Authority  Level
```cpp        
 StepBasedAuthority(CurrentPower, MinimumPower, Kt_step, delta_t);
```
| Variables | Description |
| --- | --- |
| MinimumPower | Modify this parameter to determine the threshold at which the human regains authority when applying a power. The default value is "0.01". Increase this value if you want instantaneous authority to be given to the human when a higher power is applied.|
| Kt_step | This is time coefficient of the the tank. Default value is "0.5" ranges between "0.1-2.0", increase it if you want the control shift between human and robot slower. decrease it if you want the human to regain control faster when applying an instantaneous power |


```cpp        
LinearMinMaxAuthority(CurrentPower, MinimumPower, MaximumPower, kt_lmm, delta_t);
```
| Variables | Description |
| --- | --- |
| MaximumPower | Modify this parameter to determine the maximum threshold at which the human has maximum authority. the default value is "1.2". Decreasing the value would give higher authority to the human|
| kt_lmm | Default value is "0.4" ranges between "0.1-1.0".|



```cpp        
GlobalLinearAuthority(CurrentPower, MinimumPower, GlobalPowerMean, kt_global, delta_t);
```
| Variables | Description |
| --- | --- |
| kt_global | Default value is "0.4" ranges between "0.1-1.0", increase it if you want the control shift between human and robot slower. |


## Blending
```cpp
PositionCommand = Blending(Beta, human_position, position_d_Robot, WallEnv);
```
| Variables | Description |
| --- | --- |
| Beta | This is the authority level. If you want to use step-based activity use "Beta_step" , for LinearMinMaxAuthority use "Beta_linear", for GlobalLinearAuthority use "Beta_global",etc... |
| WallEnv | This is the workspace dimensions, pre-defined and limits the blending output to get out of this dimensions|


## PD and Saturated Velocity
```cpp
 xdd_star = PDsaturated(saturated_linear_velocity, saturated_angular_velocity, err, d_gains, p_gains, xd,trajectory.interface->getCartesianAcceleration());
 ```
 | Variables | Description |
 | --- | --- |
 | saturated_linear_velocity | This is the maximum translation velocity  x,y,z vector. default value <0.4>m/s |
 | saturated_angular_velocity | This is the maximum angular velocity  wx,wy,wz vector.defaults value <0.6 rad/s>|
 | d_gains | This is the proportional gains of PD to generate a desired task acceleration. Predefined in panda_qp_control/config/torque_control.yaml |
 | p_gains | This is damping gains. Predefined in panda_qp_control/config/torque_control.yaml |


## Misc.
A PD force guidance used for the haptic device at the start of the teleoperation as a safety :

```cpp
ForceGuidance(JoyPos,FixedStart,HapticVelo,Kpf,Kdf);
```
| Variables | Description |
| --- | --- |
| FixedStart | This is the desired haptic position start we want the haptic device to stay fixed around. Default value [0.175,0.0,0.05]|
| Kpf | This is proportional gains for the force PD. default value= 70.0|
| Kdf | This is damping gains for the force PD. default value= 1.5|



## RUNNING THE CODE

$ roslaunch panda_qp_control torque_control.launch sim:=true load_gripper:=true
